# GitLab Performance SiteSpeed (beta)

SiteSpeed CI pipelines for Quality Performance testing.

A sister pipeline to [GPT's](https://gitlab.com/gitlab-org/quality/performance) backend performance pipelines, these pipelines are designed to specifically test web page frontend performance in browsers.

To get more detailed information about the current test pages list you can refer to the [Current Test Details wiki page](https://gitlab.com/gitlab-org/quality/performance-sitespeed/-/wikis/Current-Test-Details)

[[_TOC_]]

## Preparing Tests

To run SiteSpeed tests they require a text file list of URLs to test with. This file can be either created manually or prepared with the URL Generator tool.

### Preparing the Environment File

An [Environment Config File](./sitespeed/environments) is used by URL Generator tool to generate text file list of URLs that SiteSpeed will use. This file contains details about the environment to be tested.

Typically all that's needed to be done for this file is to copy one of the existing examples and tweak it accordingly for your environment. As an example here is one the example files available, [`10k.json`](./sitespeed/environments/10k.json), with details after on what specific sections you would and would not typically need to tweak:

```json
{
  "name": "10k",
  "url": "http://10k.testbed.gitlab.net"
}
```

Details for each of the settings are as follows:

* `name` - The name of the environment. (Environment variable: `ENVIRONMENT_NAME`)
* `url` - Full URL of the environment. (Environment variable: `ENVIRONMENT_URL`)

### Running the URL Generator tool

When the environment config file is in place the [URL Generator](./bin/generate-sitespeed-urls) tool can now be run to setup the data for the SiteSpeed.

Before running some setup is required for the GPT Data Generator tool specifically:

1. First, set up [`Ruby`](https://www.ruby-lang.org/en/documentation/installation/) and [`Ruby Bundler`](https://bundler.io) if they aren't already available on the machine.
    * [Ruby version](https://gitlab.com/gitlab-org/quality/performance-sitespeed/-/blob/master/.ruby-version)
    * Bundler version specified in [Gemfile.lock](https://gitlab.com/gitlab-org/quality/performance-sitespeed/-/blob/master/Gemfile.lock) below the text `BUNDLED WITH`.
1. Next, install the required Ruby Gems via Bundler
    * `bundle install`
1. If you made a custom [environment config file](#preparing-the-environment-file), add it to the `environments` directory in the tool's root folder: [`sitespeed/environments`](./sitespeed/environments).

Once setup is done you can run the tool with the `bin/generate-sitespeed-urls` script. The full options for running the tool can be seen by getting the help output via `bin/generate-sitespeed-urls --help`:

```text
Usage: generate-sitespeed-urls [options]
Options:
  -e, --environment=<s>    Name of Environment Config file in environments directory that the test(s) will be run with. Alternative filepath can also be given.
  -u, --urls-config=<s>    Name of URLs Config file in urls directory that contains the list of URLs to test with. (Default: gpt.json)
  -h, --help               Show this help message

Examples:
   bin/generate-sitespeed-urls --environment staging.json
```

By default the tool will use the [`gpt.json`](./sitespeed/urls/gpt.json) URLs Config file to generate URLs file for the SiteSpeed. Custom URLs Config file can also be provided.

After running the URLs file will be in the `urls` folder.

### SiteSpeed Configuration files

You can also specify a [config file to pass to SiteSpeed itself](https://www.sitespeed.io/documentation/sitespeed.io/configuration/#configuration-as-json). A few settings are already configured in this project's [`config.json`](./config.json) file:

```json
{
  "browsertime": {
    "browser": "chrome",
    "cpu": true,
    "iterations": "2",
    "screenshot": true,
    "screenshotParams": {
      "type": "jpg"
    },
    "videoParams": {
      "createFilmstrip": false
    },
    "viewPort": "1366x1536"
  },
  "budget": {
    "configPath": "budget.json",
    "suppressExitCode": true
  },
  "summary-detail": true
}
```

The full list of SiteSpeed config options can be found [here](https://www.sitespeed.io/documentation/sitespeed.io/configuration/#the-options).

Additional options that are used in GitLab Performance SiteSpeed pipeline:

* [`budget.json`](./budget.json) file specifies several test thresholds for all pages. Refer to the SiteSpeed [Budget documentation](https://www.sitespeed.io/documentation/sitespeed.io/performance-budget/) to learn more.
* [`sitespeed/plugin/index.js`](./sitespeed/plugin/index.js) is a custom [SiteSpeed plugin](https://www.sitespeed.io/documentation/sitespeed.io/plugins/) to export specific results into a JSON file that can be parsed by report scripts ([`ci-report-results-wiki`](./bin/ci-report-results-wiki) and [`ci-report-results-slack`](./bin/ci-report-results-slack)).

## Running Tests

The SiteSpeed tests are run via the [official Docker image](https://hub.docker.com/r/sitespeedio/sitespeed.io/).

Here is an example of the tests running against the Staging environment:

```bash
docker pull sitespeedio/sitespeed.io
docker run --shm-size=1g --rm -v "$(pwd)":/sitespeed.io sitespeedio/sitespeed.io --config config.json --plugins.add ./sitespeed/plugin --preURL http://10k.testbed.gitlab.net/explore --outputFolder results urls/10k.txt
```

To run against a different environment [prepare URLs file](#preparing-tests) and change the text file given at the end of command accordingly.

## Test Output and Results

Results will be found on the host in the folder `results`, which will be located in the same directory as the one you used the command in.

Once all tests have completed you will be presented with the default [SiteSpeed results summary](https://www.sitespeed.io/examples/).

As a convenience we provide a way to generate a custom results table for the test run that highlights key metrics. To generate the table run `bin/ci-report-results-wiki --dry-run` command. As an example, here is a test summary for all tests done against the `10k` environment:

```txt
NAME                                  | FCP (ms) | LCP (ms)         | TBT (ms)         | SI (ms) | LVC (ms) | TFR SIZE (kb) | SCORE | RESULT
--------------------------------------|----------|------------------|------------------|---------|----------|---------------|-------|-------
web_group                             | 887      | ✓ 887 (<2500)    | ✓ 614 (<1100)    | 985     | 1967     | 67.4          | 89    | Passed
web_project                           | 1897     | ✓ 1897 (<2500)   | ✓ 1784 (<2250)   | 2324    | 5733     | 277.1         | 78    | Passed
web_project_branches                  | 1509     | ✓ 1509 (<2500)   | ✓ 109 (<300)     | 1735    | 2050     | 34.5          | 93    | Passed
web_project_commit                    | 11989    | ✓ 11989 (<12500) | ✓ 6013 (<7500)   | 15719   | 20850    | 328.0         | 79    | Passed
web_project_commits                   | 2163     | ✓ 2163 (<2500)   | ✓ 270 (<350)     | 2550    | 2550     | 59.9          | 88    | Passed
web_project_file_blame                | 3652     | ✓ 3652 (<4500)   | ✓ 28691 (<32500) | 4111    | 34133    | 935.5         | 87    | Passed
web_project_file_rendered             | 784      | ✓ 18890 (<20000) | ✓ 2296 (<3000)   | 1376    | 11650    | 522.8         | 80    | Passed
web_project_file_source               | 950      | ✓ 21924 (<27500) | ✓ 16069 (<20000) | 2746    | 23350    | 878.4         | 85    | Passed
web_project_files                     | 1380     | ✓ 2050 (<2500)   | ✓ 8100 (<10000)  | 2284    | 8083     | 294.5         | 77    | Passed
web_project_issue                     | 2305     | ✓ 4390 (<5000)   | ✓ 1678 (<3500)   | 2512    | 4317     | 423.0         | 73    | Passed
web_project_issues                    | 821      | ✓ 821 (<2500)    | ✓ 308 (<400)     | 986     | 1267     | 137.1         | 78    | Passed
web_project_merge_request_changes     | 1266     | ✓ 8726 (<10000)  | ✓ 26586 (<32500) | 6827    | 13350    | 1362.9        | 73    | Passed
web_project_merge_request_commits     | 1574     | ✓ 1574 (<2500)   | ✓ 949 (<1250)    | 2389    | 3567     | 933.0         | 75    | Passed
web_project_merge_request_discussions | 1312     | ✓ 2243 (<2500)   | ✓ 3046 (<4000)   | 1856    | 8217     | 864.6         | 75    | Passed
web_project_merge_requests            | 753      | ✓ 753 (<2500)    | ✓ 188 (<400)     | 869     | 1067     | 82.2          | 84    | Passed
web_project_pipelines                 | 450      | ✓ 2717 (<3500)   | ✓ 733 (<1250)    | 848     | 2783     | 127.2         | 80    | Passed
web_user                              | 1025     | ✓ 1910 (<2500)   | ✓ 892 (<1250)    | 1461    | 2750     | 46.1          | 86    | Passed
```

Through these tests we measure several modern key metrics, recommended by [Google and the W3C](https://web.dev/metrics/) that indicate how pages are performing. We also follow the recommended targets for these metrics where specified:

* `FCP` - [First Contentful Paint (FCP)](https://web.dev/fcp/) measures the time from when the page starts loading to when any part of the page's content is rendered on the screen.
* `LCP` - [Largest Contentful Paint (LCP)](https://web.dev/lcp/) reports the render time of the largest image or text block visible within the viewport. Recommended target - 2500ms.
* `TBT` - [Total Blocking Time (TBT)](https://web.dev/tbt/) measures the total amount of time where the main thread was blocked for long enough to prevent input responsiveness. Recommended target - 300ms.
* `SI` - [Speed Index (SI)](https://web.dev/speed-index/) measures how quickly content is visually displayed during page load.
* `LVC` - [Last Visual Change (LVC)](https://www.sitespeed.io/documentation/sitespeed.io/metrics/#last-visual-change) measures the time when something for the last time changes within the viewport.
* `TFR SIZE` - The [Transfer Size (TFR SIZE)](https://www.sitespeed.io/documentation/sitespeed.io/metrics/#transfer-size) over the wire in KB. Helps to evaluate response page size.
* `SCORE` - The [Coach Performance Score (SCORE)](https://www.sitespeed.io/documentation/coach/introduction/) given by SiteSpeed for the page.

We also post SiteSpeed results over on GPT [project's wiki](https://gitlab.com/gitlab-org/quality/performance/wikis/home) for transparency as well as allowing users to compare:

* [SiteSpeed Results](https://gitlab.com/gitlab-org/quality/performance/-/wikis/Benchmarks/SiteSpeed) - Our automated CI pipelines run multiple times each week and will post their result summaries to the wiki here each time.

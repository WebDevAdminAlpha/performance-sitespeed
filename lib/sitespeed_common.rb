require 'http'
require 'json'
require 'table_print'

module SiteSpeedCommon
  extend self

  def make_http_request(method: 'get', url: nil, params: {}, headers: {}, body: "", show_response: false, fail_on_error: true)
    raise "URL not defined for making request. Exiting..." unless url

    ctx = OpenSSL::SSL::SSLContext.new
    ctx.verify_mode = OpenSSL::SSL::VERIFY_NONE

    res = body.empty? ? HTTP.follow.method(method).call(url, form: params, headers: headers, ssl_context: ctx) : HTTP.follow.method(method).call(url, body: body, headers: headers, ssl_context: ctx)

    if show_response
      res_body = res.content_type.mime_type == "application/json" ? JSON.parse(res.body.to_s) : res.body.to_s
      puts res_body
    end

    raise "#{method.upcase} request failed!\nCode: #{res.code}\nResponse: #{res.body}\n" if fail_on_error && !res.status.success?

    res
  end

  def find_test_name(urls_config, value)
    name = value
    urls_config.each do |test_name, url_config|
      name = test_name if url_config.key(value)
    end
    name
  end

  def get_test_thresholds(test_url_config)
    thresholds = {}
    return thresholds if test_url_config.nil?

    thresholds["LCP"] = ENV['LARGEST_CONTENTFUL_PAINT_THRESHOLD'].dup || 2500
    thresholds["TBT"] = ENV['TOTAL_BLOCKING_TIME_THRESHOLD'].dup || 300

    return thresholds unless test_url_config.key?('thresholds')

    test_url_config['thresholds'].each do |thr_name, thr_value|
      thresholds[thr_name] = thr_value
    end

    thresholds
  end

  def get_test_results(test_result:, urls_config:)
    results = Hash.new('-')

    results['name'] = find_test_name(urls_config, test_result['subject'])

    test_result['metrics'].each do |metric|
      case metric["name"]
      when /First Contentful Paint/
        results["FCP"] = metric['value']
      when /Largest Contentful Paint/
        results["LCP"] = metric['value']
      when /Total Blocking Time/
        results["TBT"] = metric['value']
      when /Speed Index/
        results["speed_index"] = metric['value']
      when /Last Visual Change/
        results["LVC"] = metric['value']
      when /Transfer Size\s*/
        results["transfer_size"] = metric['value']
      when /Coach Performance Score*/
        results["coach_performance_score"] = metric['value']
      end
    end

    test_thresholds = get_test_thresholds(urls_config.dig(results['name']))
    test_thresholds.each do |thr_name, thr_value|
      results["result"] &&= results[thr_name].to_f < thr_value.to_f
    end

    results
  end

  def get_tests_results(results_file:, urls_config:)
    test_results = []

    results_json = JSON.parse(File.read(results_file))
    results_json.each do |test_result|
      test_results << get_test_results(test_result: test_result, urls_config: urls_config)
    end

    test_results
  end

  def get_result_threshold_table_string(test_result:, test_threshold: '?')
    result = test_result.to_f < test_threshold.to_f
    results_table_string = "#{result ? '✓' : '✘'} #{test_result}"
    results_table_string << " (<#{test_threshold.nil? ? '?' : test_threshold})"
    results_table_string
  end

  def generate_results_table(test_results:, urls_config:)
    tp_results = test_results.map do |test_result|
      tp_result = Hash.new('-')
      test_thresholds = get_test_thresholds(urls_config[test_result['name']])

      tp_result["NAME"] = test_result['name']
      tp_result["FCP (ms)"] = test_result['FCP']
      tp_result["LCP (ms)"] = get_result_threshold_table_string(test_result: test_result['LCP'], test_threshold: test_thresholds['LCP'])
      tp_result["TBT (ms)"] = get_result_threshold_table_string(test_result: test_result['TBT'], test_threshold: test_thresholds['TBT'])
      tp_result["SI (ms)"] = test_result['speed_index']
      tp_result["LVC (ms)"] = test_result['LVC']
      tp_result["TFR SIZE (kb)"] = test_result['transfer_size']
      tp_result["SCORE"] = test_result['coach_performance_score']
      tp_result["RESULT"] = test_result['result'] ? "Passed" : "FAILED"

      tp_result
    end

    tp.set(:capitalize_headers, false)
    tp.set(:max_width, 60)
    TablePrint::Printer.table_print(tp_results)
  end

  def get_aggregated_results(test_results)
    test_results.reduce(true) { |aggregated, test_result| aggregated && test_result['result'] }
  end
end
